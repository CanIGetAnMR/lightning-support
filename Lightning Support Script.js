/**
 * Script Name: Lightning Support
 * Script Purpose: Created for PMS / Crunch on world 112. And of course everyone else as soon as en112 finishes! :]
 * Version: 1.2
 * Author: Frying Pan Warrior
 *
 * Description: Uses as many villages as possible to send support to a target.
 *
 *  It will use spears and swords if it can.
 *  It will start using heavy cavalry if there is not enough swords.
 *  Display number of troops / packets left after sending this round.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 */
{
    let version = "1.2";

    let leave_at_least_this_many_spears = 500;
    let leave_at_least_this_many_swords = 500;
    let leave_at_least_this_many_heavy = 100;
    let number_of_packets_to_send = 5;
    let use_heavy_to_replace_swords = true;

    let number_of_spears_in_a_packet = 1000;
    let number_of_swords_in_a_packet = 1000;
    let number_of_swords_a_heavy_can_replace = 4;

    const villageTableId = "village_troup_list";
    const troopNameNodeName = "data-unit";
    const troopInputClassName = "call-unit-box";
    const displayElementId = "lightning_support_display";
    const spearCheckBoxId = "checkbox_spear";
    const swordCheckBoxId = "checkbox_sword";
    const heavyCheckBoxId = "checkbox_heavy";
    const selectAllCheckBoxId = "place_call_select_all";

    const spearName = "spear";
    const swordName = "sword";
    const heavyName = "heavy";

    const UnitType = {
        spear: spearName,
        sword: swordName,
        heavy: heavyName
    };

    const troopMinimums = {};
    troopMinimums[spearName] = leave_at_least_this_many_spears;
    troopMinimums[swordName] = leave_at_least_this_many_swords;
    troopMinimums[heavyName] = leave_at_least_this_many_heavy;

    function Village() {
        this.formReference = {
            spear: null,
            sword: null,
            heavy: null
        }
        this.units = {
            spear: 0,
            sword: 0,
            heavy: 0
        }
        this.contribution = {
            spear: 0,
            sword: 0,
            heavy: 0
        }
    }

    function DistributionResult(remainingSpearCount, remainingSwordCount) {
        this.remainingSpearCount = remainingSpearCount;
        this.remainingSwordCount = remainingSwordCount;
        this.spear = 0;
        this.sword = 0;
        this.heavy = 0;
    }

    function CheckBoxStates(spear, sword, axe, spy, light, heavy, ram, cat, paladin) {
        this.spear = spear;
        this.sword = sword;
        this.axe = axe;
        this.spy = spy;
        this.light = light;
        this.heavy = heavy;
        this.ram = ram;
        this.cat = cat;
        this.paladin = paladin;
    }

    function getTroopTableRows() {
        let troopTable = window.document.getElementById(villageTableId);
        let troopTableBody = troopTable.tBodies[0];
        return troopTableBody.children;
    }

    function getCheckBoxStates() {
        let spearCheckbox = document.getElementById("checkbox_spear");
        let swordCheckbox = document.getElementById("checkbox_sword");
        let axeCheckbox = document.getElementById("checkbox_axe");
        let spyCheckbox = document.getElementById("checkbox_spy");
        let lightCheckbox = document.getElementById("checkbox_light");
        let heavyCheckbox = document.getElementById("checkbox_heavy");
        let ramCheckbox = document.getElementById("checkbox_ram");
        let catCheckbox = document.getElementById("checkbox_catapult");
        let paladinCheckbox = document.getElementById("checkbox_knight");

        return new CheckBoxStates(
            spearCheckbox.checked,
            swordCheckbox.checked,
            axeCheckbox.checked,
            spyCheckbox.checked,
            lightCheckbox.checked,
            heavyCheckbox.checked,
            ramCheckbox.checked,
            catCheckbox.checked,
            paladinCheckbox.checked
        );
    }

    function disableCheckBoxes() {
        let checkboxElements = [
            document.getElementById("checkbox_spear"),
            document.getElementById("checkbox_sword"),
            document.getElementById("checkbox_axe"),
            document.getElementById("checkbox_spy"),
            document.getElementById("checkbox_light"),
            document.getElementById("checkbox_heavy"),
            document.getElementById("checkbox_ram"),
            document.getElementById("checkbox_catapult"),
            document.getElementById("checkbox_knight")
        ];

        for (let checkboxElement of checkboxElements) {
            if (checkboxElement.checked) {
                checkboxElement.click();
            }
        }
    }

    function selectCheckBoxes(checkBoxIds) {
        for (let checkBoxId of checkBoxIds) {
            let checkBox = document.getElementById(checkBoxId);
            if (checkBox.checked) {
                checkBox.click();
            }
            checkBox.click();
        }
    }

    function restoreCheckBoxes(checkboxStates) {
        let spearCheckbox = document.getElementById("checkbox_spear");
        let swordCheckbox = document.getElementById("checkbox_sword");
        let axeCheckbox = document.getElementById("checkbox_axe");
        let spyCheckbox = document.getElementById("checkbox_spy");
        let lightCheckbox = document.getElementById("checkbox_light");
        let heavyCheckbox = document.getElementById("checkbox_heavy");
        let ramCheckbox = document.getElementById("checkbox_ram");
        let catCheckbox = document.getElementById("checkbox_catapult");
        let paladinCheckbox = document.getElementById("checkbox_knight");

        restoreCheckBox(spearCheckbox, checkboxStates.spear);
        restoreCheckBox(swordCheckbox, checkboxStates.sword);
        restoreCheckBox(axeCheckbox, checkboxStates.axe);
        restoreCheckBox(spyCheckbox, checkboxStates.spy);
        restoreCheckBox(lightCheckbox, checkboxStates.light);
        restoreCheckBox(heavyCheckbox, checkboxStates.heavy);
        restoreCheckBox(ramCheckbox, checkboxStates.ram);
        restoreCheckBox(catCheckbox, checkboxStates.cat);
        restoreCheckBox(paladinCheckbox, checkboxStates.paladin);
    }

    function restoreCheckBox(checkBox, checkBoxStateClicked) {
        if (checkBox.checked !== checkBoxStateClicked) {
            checkBox.click();
        }
    }

    function getAllVillages() {
        let troopTableRows = getTroopTableRows();
        let villages = [];

        for (let villageEntry of troopTableRows) {
            let village = new Village();
            let villageEntryColumnCollection = villageEntry.children;

            for (let troopColumn of villageEntryColumnCollection) {
                let columnAttributes = troopColumn.attributes;
                let troopNameNode = columnAttributes.getNamedItem(troopNameNodeName);
                if (troopNameNode != null) {
                    let inputBox = troopColumn.querySelector("." + troopInputClassName);
                    let troopName = troopNameNode.nodeValue;
                    village.formReference[troopName] = inputBox;
                    village.units[troopName] = +inputBox.value;
                }
            }
            villages.push(village);
        }

        return villages;
    }

    function distributeTroopContributions(villages) {
        let neededSpearCount = Math.ceil(number_of_packets_to_send * number_of_spears_in_a_packet);
        let neededSwordCount = Math.ceil(number_of_packets_to_send * number_of_swords_in_a_packet);
        let villageCount = villages.length;

        let averageSpearsPerVillage = neededSpearCount / villageCount;
        let averageSwordsPerVillage = neededSwordCount / villageCount;
        let averageHeavyPerVillage = averageSwordsPerVillage / number_of_swords_a_heavy_can_replace;
        averageSpearsPerVillage = averageSpearsPerVillage < 1 ? 1 : Math.floor(averageSpearsPerVillage);
        averageSwordsPerVillage = averageSwordsPerVillage < 1 ? 1 : Math.floor(averageSwordsPerVillage);
        averageHeavyPerVillage = averageHeavyPerVillage < 1 ? 1 : Math.floor(averageHeavyPerVillage);

        while (neededSpearCount > 0 || neededSwordCount > 0) {
            let neededSpearCountCheck = neededSpearCount;
            let neededSwordCountCheck = neededSwordCount;

            for (let village of villages) {
                let spearContribution = 0;
                let swordContribution = 0;
                let heavyContribution = 0;

                if (neededSpearCount > 0) {
                    spearContribution = tryUnitContribution(UnitType.spear, village, averageSpearsPerVillage);
                    neededSpearCount -= spearContribution;
                }

                if (neededSwordCount > 0) {
                    swordContribution = tryUnitContribution(UnitType.sword, village, averageSwordsPerVillage);
                    neededSwordCount -= swordContribution;

                    if (use_heavy_to_replace_swords === true) {
                        if (spearContribution > 0 && swordContribution === 0) {
                            heavyContribution = tryUnitContribution(UnitType.heavy, village, averageHeavyPerVillage);
                            neededSwordCount -= heavyContribution * number_of_swords_a_heavy_can_replace;
                        }
                    }
                }
            }

            if (neededSpearCountCheck === neededSpearCount && neededSwordCountCheck === neededSwordCount) {
                break;
            }
        }

        if (use_heavy_to_replace_swords === true) {
            while (neededSwordCount > 0) {
                let neededSwordCountCheck = neededSwordCount;

                for (let village of villages) {
                    if (neededSwordCount > 0) {
                        let heavyContribution = tryUnitContribution(UnitType.heavy, village, averageHeavyPerVillage);
                        neededSwordCount -= heavyContribution * number_of_swords_a_heavy_can_replace;
                    }
                }

                if (neededSwordCountCheck === neededSwordCount) {
                    break;
                }
            }
        }

        return new DistributionResult(neededSpearCount, neededSwordCount);
    }

    function tryUnitContribution(troopName, village, targetContribution) {
        let troopContribution = getMaximumUnitContribution(village.units[troopName], troopMinimums[troopName], targetContribution);
        village.contribution[troopName] += troopContribution;
        village.units[troopName] -= troopContribution;
        return troopContribution;
    }

    function getMaximumUnitContribution(villageUnitCount, minimumRemainingUnitCount, targetContribution) {
        let availableContribution = villageUnitCount - minimumRemainingUnitCount;
        if (availableContribution < 0) {
            availableContribution = 0;
        }
        return Math.min(availableContribution, targetContribution);
    }

    function populateDistributionCounts(villages, distributionResult) {
        for (let village of villages) {
            distributionResult[spearName] += village.contribution[spearName];
            distributionResult[swordName] += village.contribution[swordName];
            distributionResult[heavyName] += village.contribution[heavyName];
        }
    }

    function displayDistributionResult(distributionResult) {
        let missingSpears = distributionResult.remainingSpearCount;
        let missingSwords = distributionResult.remainingSwordCount;
        let totalSpears = distributionResult[spearName];
        let totalSwords = distributionResult[swordName];
        let totalHeavy = distributionResult[heavyName];

        let infoElement = document.createTextNode(`Total troops selected - ${spearName}: ${totalSpears}  ${swordName}:  ${totalSwords}  ${heavyName}: ${totalHeavy}`);
        let displayElement = document.getElementById(displayElementId);
        displayElement.appendChild(infoElement);

        if (missingSpears > 0) {
            let warningElement = document.createTextNode("Lack of " + missingSpears + " to fulfill packets.");
            let displayElement = document.getElementById(displayElementId);
            displayElement.appendChild(warningElement);
        }

        if (missingSwords > 0) {
            let warningElement = document.createTextNode("Lack of " + missingSwords + " to fulfill packets.");
            let displayElement = document.getElementById(displayElementId);
            displayElement.appendChild(warningElement);
        }
    }

    function insertTroopCountsIntoForm(villages) {
        for (let village of villages) {
            for (let troopName in village.contribution) {
                let contributionAmount = village.contribution[troopName];
                let inputBox = village.formReference[troopName];
                inputBox.value = contributionAmount.toString();
            }
        }
    }

    function initDisplay() {
        let formShowEmpty = document.getElementById("show_empty");

        let displayElement = document.createElement("div");
        displayElement.setAttribute("style", "float:left; width:60%; text-align:center");
        displayElement.setAttribute("id", displayElementId);
        formShowEmpty.insertBefore(displayElement, formShowEmpty.firstChild);

        let titleElement = document.createElement("h2");
        titleElement.textContent = `Lightning Support v${version}`;
        let descriptionElement = document.createElement("h3");
        descriptionElement.textContent = "For PMS / Crunch, by Alex (Frying Pan Warrior)";
        displayElement.appendChild(titleElement);
        displayElement.appendChild(descriptionElement);
    }

    function clearDisplay() {
        let displayElement = document.getElementById(displayElementId);
        if (displayElement != null) {
            let formShowEmpty = document.getElementById("show_empty");
            formShowEmpty.removeChild(displayElement);
        }
    }

    function isCorrectURL() {
        let urlParameters = new URLSearchParams(window.location.search);
        let screen = urlParameters.get("screen");
        let mode = urlParameters.get("mode");
        return screen === "place" && mode === "call"
    }

    function switchToMassSupportUrl() {
        let urlParameters = new URLSearchParams(window.location.search);
        urlParameters.set("screen", "place");
        urlParameters.set("mode", "call");

        window.location.search = urlParameters.toString();
    }

    function mainLightningSupport() {
        if (!isCorrectURL()) {
            switchToMassSupportUrl();
            return;
        }

        try {
            clearDisplay();
            initDisplay();
            let checkBoxStates = getCheckBoxStates();
            disableCheckBoxes();
            selectCheckBoxes([spearCheckBoxId, swordCheckBoxId, heavyCheckBoxId, selectAllCheckBoxId]);
            let sourceVillages = getAllVillages();
            let distributionResult = distributeTroopContributions(sourceVillages);
            populateDistributionCounts(sourceVillages, distributionResult);
            displayDistributionResult(distributionResult);
            insertTroopCountsIntoForm(sourceVillages);
            restoreCheckBoxes(checkBoxStates);
        } catch (e) {
            UI.ErrorMessage("An error has occurred please contact the script maintainer.");
            console.log(e);
        }

    }

    mainLightningSupport();
}