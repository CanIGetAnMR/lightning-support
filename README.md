Lightning Support
=================

Request back-line support to your front line villages in an efficient manner.

Approval Thread: [https://forum.tribalwars.net/index.php?threads/286886/](https://forum.tribalwars.net/index.php?threads/lightning-support.286886/)
