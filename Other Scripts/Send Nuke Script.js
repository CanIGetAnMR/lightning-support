/**
 * Script Name: Send Nuke
 * Script Purpose: Quickly sending your OP's nukes out untimed because ain't nobody got the time for that.
 * Version: 0.9
 * Author: Frying Pan Warrior
 *
 * Description: Parses a list of nuke sends and then determines if the currently selected village has a target. If so it selects all offensive units and fills in the target coordinate. Use in rally point.
 *
 *  I was using fxutility fodox attack planner to generate the attack plan. Then I used notepad++ with some regex to convert the targets to "xxx|xxx->xxx|xxx\n" + like nukevillage->targetvillage
 *  which goes into the instructions variable then gets used by the script. That comment //Strip regex at line 110 was most likely what I was using in notepad++.
 *  I used my nuke group and then pushed this script, sent the nuke and then switched to next village until all nukes were sent.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 */

javascript:{//Frying Pan Warrior
    let currentVillage = game_data.village.coord;
    let instructions = "347|521->400|660\n" +
        "343|523->399|659\n" +
        "392|514->400|660\n" +
        "351|521->399|659\n" +
        "400|515->400|660\n" +
        "381|516->398|660\n" +
        "354|522->398|660\n" +
        "361|520->398|660\n" +
        "392|515->399|659\n" +
        "362|520->397|657\n" +
        "385|517->397|657\n" +
        "391|517->397|657\n" +
        "373|518->402|651\n" +
        "369|519->402|651\n" +
        "389|518->399|652\n" +
        "380|519->399|652\n" +
        "382|519->399|652\n" +
        "346|527->399|650\n" +
        "392|518->402|651\n" +
        "380|520->399|650\n" +
        "382|520->399|650\n" +
        "387|537->401|658\n" +
        "383|538->401|658\n" +
        "347|528->398|638\n" +
        "387|538->401|658\n" +
        "360|524->398|638\n" +
        "368|522->398|638\n" +
        "351|527->398|637\n" +
        "352|527->398|637\n" +
        "379|521->398|637\n" +
        "349|528->396|631\n" +
        "350|528->396|631\n" +
        "361|526->392|634\n" +
        "359|527->392|634\n" +
        "355|527->396|631\n" +
        "389|523->392|634\n" +
        "390|524->385|633\n" +
        "354|529->385|633\n" +
        "387|525->385|633\n" +
        "360|529->388|632\n" +
        "383|526->388|632\n" +
        "390|526->388|632\n" +
        "389|526->381|628\n" +
        "387|526->381|628\n" +
        "388|528->383|628\n" +
        "386|528->381|628\n" +
        "387|528->383|628\n" +
        "384|529->383|628\n" +
        "375|532->385|627\n" +
        "371|533->385|627\n" +
        "385|532->385|627\n" +
        "389|533->387|627\n" +
        "385|533->387|627\n" +
        "377|533->387|626\n" +
        "378|533->387|626\n" +
        "391|533->387|626\n" +
        "388|535->389|628\n" +
        "377|535->388|627\n" +
        "376|535->387|627\n" +
        "378|535->388|627\n" +
        "379|536->389|628\n" +
        "390|535->388|627\n" +
        "388|536->389|628\n";

    let listInstructions = instructions.split("\n");
    let processedInstructionArray = [];
    listInstructions.forEach(function (item, index, array) {
        let itemPair = item.split("->");
        processedInstructionArray.push(itemPair);
    });
    for (let item of processedInstructionArray) {
        let sourceVill = item[0];
        if (currentVillage === sourceVill) {
            let coords = item[1].split('|');
            let troopForm = document.getElementById("command-data-form");
            troopForm.x.value = coords[0];
            troopForm.y.value = coords[1];
            $('#place_target').find('input').val(coords[0] + '|' + coords[1]);
            troopForm.spy.value = troopForm.spy.dataset.allCount;
            troopForm.axe.value = troopForm.axe.dataset.allCount
            troopForm.light.value = troopForm.light.dataset.allCount;
            troopForm.ram.value = troopForm.ram.dataset.allCount;
            troopForm.catapult.value = troopForm.catapult.dataset.allCount;
            troopForm.knight.value = troopForm.knight.dataset.allCount;
            troopForm.sword.value = troopForm.sword.dataset.allCount;
            troopForm.spear.value = troopForm.spear.dataset.allCount;
            troopForm.heavy.value = troopForm.heavy.dataset.allCount;
        }
    }
    //Strip regex Ram\s+Attack\s+\d\d-\d\d-\d\d\d\d\s\d\d\:\d\d:\d\d.\d\d\d
}