/**
 * Script Name: Market Mint Script
 * Script Purpose: Sending a balanced amount of resources to your minting village so you dont end up with issues where you have too much of a particular resource.
 * Version: 0.8
 * Author: Frying Pan Warrior
 *
 * Description: Fills in the market send resources fields with a balanced amount of resources based on the inputted woodCost, clayCost, ironCost variables.
 *
 * Disclaimer: This code appears to have debug code at lines 43 and 44 that sends a set amount of resources to a particular village. Just ignore those two lines at the bottom and don't use them.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 */

javascript: //This defines the below script as javascript
woodCost = 15680; //The wood cost of a coin
clayCost = 16800; //The clay cost of a coin
ironCost = 14000; //The iron cost of a coin
villageTarget = '611|555'; //The minting village
end_of_user_modifiable_variables='this_script_was_created_by_frying_pan_warrior'; //This is a comment variable
doc=document; //This assigns the document reference to a shorter variable called doc
if(window.frames.length>0 && window.main!=null)doc=window.main.document; //This is some boiler plate I copy pasted from the fake script that I hope had a license that allowed me to do so.
combinedCost=woodCost+clayCost+ironCost; //This adds all the resources together so we can later calculate the number of merchants needed
maxTransfer=doc.getElementById('market_merchant_max_transport').innerHTML; //This scrapes the maximum number of resources that can be transported from the webpage (based on merchant count)
multiplier=maxTransfer/combinedCost; //This figures out how many "coins" worth of resources we can send based on the max transport amount
food=woodCost*multiplier; //This figures out how much wood we can send based on the wood cost of the number of coins we can send
wood=clayCost*multiplier; //This figures out how much clay we can send based on the clay cost of the number of coins we can send
coin=ironCost*multiplier; //This figures out how much iron we can send based on the iron cost of the number of coins we can send
doc.forms[0].wood.value=Math.floor(food); //Then we round down to nearest resource AND POP THAT VALUE INTO THE send wood field
doc.forms[0].stone.value=Math.floor(wood); //Then we round down to nearest resource AND POP THAT VALUE INTO THE send clay field
doc.forms[0].iron.value=Math.floor(coin); //Then we round down to nearest resource AND POP THAT VALUE INTO THE send iron field
$('#market_target').find('input').val(villageTarget); //Then we FILL IN the target village

//Now the user is all set to click send and can now repeat this script in every village they have except the minting one and send a wonderful balanced amount of coin minting resources from each village :)
//Made by Alex  aka Frying Pan Warrior aka xAlexzx and that boiler plate code from the fake script.
//Hi Woofie.




$("#textareaID").val()
javascript:food=69494;wood=74457;coin=62048;var doc=document;if(window.frames.length>0 && window.main!=null)doc=window.main.document;url=doc.URL;doc.forms[0].wood.value=food;doc.forms[0].stone.value=wood;doc.forms[0].iron.value=coin;$('#market_target').find('input').val('611|555');