/**
 * Script Name: Hidden Fang
 * Script Purpose: Sending hidden fangs on a Watch Tower world such as en112.
 * Version: 1.0
 * Author: Frying Pan Warrior
 *
 * Description: Fills the map send units popup with as many cats and strongest units as possible while remaining under 1000 units to look like a fake.
 *
 *  Change maxRamCount to whatever you like.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 */

{
    let author = "Frying Pan Warrior (Alex)";

    let unitsRemaining = 1000;

    const maxRamCount = 60;

    let totalRamsAvailable = +document.getElementById("unit_input_ram").getAttribute("data-all-count");
    let totalCatsAvailable = +document.getElementById("unit_input_catapult").getAttribute("data-all-count");
    let totalHCsAvailable = +document.getElementById("unit_input_heavy").getAttribute("data-all-count");
    let totalLCsAvailable = +document.getElementById("unit_input_light").getAttribute("data-all-count");
    let totalScoutsAvailable = +document.getElementById("unit_input_spy").getAttribute("data-all-count");

    let scoutsUsed = totalScoutsAvailable > 0 ? 1 : 0;
    unitsRemaining -= scoutsUsed;

    let ramsUsed = totalRamsAvailable <= maxRamCount ? totalRamsAvailable : maxRamCount;
    unitsRemaining -= ramsUsed;

    let catsUsed = totalCatsAvailable <= unitsRemaining ? totalCatsAvailable : unitsRemaining;
    unitsRemaining -= catsUsed;

    let hcsUsed = 0;
    if (unitsRemaining > 0) {
        hcsUsed = totalHCsAvailable <= unitsRemaining ? totalHCsAvailable : unitsRemaining;
        unitsRemaining -= hcsUsed;
    }

    let lcsUsed = 0;
    if (unitsRemaining > 0) {
        lcsUsed = totalLCsAvailable <= unitsRemaining ? totalLCsAvailable : unitsRemaining;
        unitsRemaining -= lcsUsed;
    }

    if (unitsRemaining > 0) {
        let ramIncrease = totalRamsAvailable - ramsUsed <= unitsRemaining ? totalRamsAvailable - ramsUsed : unitsRemaining;
        ramsUsed += ramIncrease;
        unitsRemaining -= ramIncrease;
    }

    if (unitsRemaining > 0) {
        let scoutIncrease = totalScoutsAvailable - scoutsUsed <= unitsRemaining ? totalScoutsAvailable - scoutsUsed : unitsRemaining;
        scoutsUsed += scoutIncrease;
        unitsRemaining -= scoutIncrease;
    }

    let troopForm = document.getElementById("command-data-form");

    troopForm.spy.value = scoutsUsed;
    troopForm.ram.value = ramsUsed;
    troopForm.catapult.value = catsUsed;
    troopForm.light.value = lcsUsed;
    troopForm.heavy.value = hcsUsed;
}